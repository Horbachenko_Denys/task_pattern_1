package com.epam.view;


import com.epam.controller.impl.KyivBakery;
import com.epam.controller.impl.LvivBakery;
import com.epam.enums.PizzaTypes;
import com.epam.model.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class MyView {

    public static Logger logger = LogManager.getLogger(MyView.class);
    private static Scanner input = new Scanner(System.in);
    private PizzaTypes pizzaTypes;
    private Pizza pizza;

    public MyView() {
        while (true) {
            System.out.println("==== Order pizza Menu: ====");
            int city = chooseCity();
            int pizzaType = choosePizzaType();
            int pizzaQuantity = choosePizzaQuantity();
            makePizza(city, pizzaType, pizzaQuantity);
            getBill(pizza, pizzaQuantity);
        }
    }

    private int chooseCity() {
        logger.info("Choose your city: \n " +
                "1 - Kyiv \n " +
                "2 - Lviv \n " +
                "3 - Dnipro");
        return input.nextInt();
    }

    private int choosePizzaType() {
        logger.info("Choose pizza type: \n " +
                "1 - Cheese \n " +
                "2 - Pepperoni \n " +
                "3 - Clam \n " +
                "4 - Veggie");
        return input.nextInt();
    }

    private int choosePizzaQuantity() {
        logger.info("Choose pizza quantity:");
        return input.nextInt();
    }

    private void getBill(Pizza pizza, int quantity) {
        logger.info(pizza.getComponents());
        logger.info("Price: " + quantity * pizza.getPrice() + "$");
    }

    public void makePizza(int city, int pizzaType, int pizzaQuantity) {
        switch (pizzaType) {
            case 1:
                pizzaTypes = PizzaTypes.CheesePizza;
                break;
            case 2:
                pizzaTypes = PizzaTypes.PepperoniPizza;
                break;
            case 3:
                pizzaTypes = PizzaTypes.ClamPizza;
                break;
            case 4:
                pizzaTypes = PizzaTypes.VeggiePizza;
                break;
        }

        for (int i = 0; i < pizzaQuantity; i++) {
            if (city == 1) {
                pizza = new KyivBakery().makePizza(pizzaTypes);
            }
            if (city == 2) {
                pizza = new LvivBakery().makePizza(pizzaTypes);
            }
            if (city == 3) {
                pizza = new LvivBakery().makePizza(pizzaTypes);
            }
        }
    }
}
