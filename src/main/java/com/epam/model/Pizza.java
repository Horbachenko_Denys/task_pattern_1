package com.epam.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Pizza {

   private static int price;
   private int pricePerInstance;
   private List<String> components = new ArrayList<>();

    public Pizza(String...s) {
        this.components = Arrays.asList(s);
    }

    public List<String> getComponents() {
        return components;
    }

    public int getPrice() {
        return price;
    }

    public abstract void make();
    public abstract void bake();
    public abstract void box();
}