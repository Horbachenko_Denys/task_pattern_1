package com.epam.model.impl;

import com.epam.model.Pizza;
import com.epam.view.MyView;

import java.util.Arrays;
import java.util.List;

public class PeperoniPizza extends Pizza {

    private static int price = 3;
    private List<String> components;

    public PeperoniPizza(String...s) {
        this.components = Arrays.asList(s);
    }

    public List<String> getComponents() {
        return components;
    }

    public int getPrice() {
        return price;
    }


    @Override
    public void make() {
        MyView.logger.info("Making peperoni pizza...");
    }

    @Override
    public void bake() {
        MyView.logger.info("Baking peperoni pizza...");
    }

    @Override
    public void box() {
        MyView.logger.info("Boxing peperoni pizza...");
    }
}
