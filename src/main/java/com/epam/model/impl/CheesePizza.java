package com.epam.model.impl;

import com.epam.model.Pizza;
import com.epam.view.MyView;

import java.util.Arrays;
import java.util.List;

public class CheesePizza extends Pizza {

    private static int price  = 4;
    private List<String> components;

    public CheesePizza(String... s) {
        this.components = Arrays.asList(s);
    }

    public List<String> getComponents() {
        return components;
    }

    public int getPrice() {
        return price;
    }


    @Override
    public void make() {
        MyView.logger.info("Making cheese pizza...");
    }

    @Override
    public void bake() {
        MyView.logger.info("Baking cheese pizza...");
    }

    @Override
    public void box() {
        MyView.logger.info("Boxing cheese pizza...");
    }
}
