package com.epam.model.impl;

import com.epam.model.Pizza;
import com.epam.view.MyView;

import java.util.Arrays;
import java.util.List;

public class ClamPizza extends Pizza {

    private static int price  = 6;
    private List<String> components;

    public ClamPizza(String...s) {
        this.components = Arrays.asList(s);
    }

    public List<String> getComponents() {
        return components;
    }

    public int getPrice() {
        return price;
    }


    @Override
    public void make() {
        MyView.logger.info("Making clam pizza...");
    }

    @Override
    public void bake() {
        MyView.logger.info("Baking clam pizza...");
    }

    @Override
    public void box() {
        MyView.logger.info("Boxing clam pizza...");
    }
}
