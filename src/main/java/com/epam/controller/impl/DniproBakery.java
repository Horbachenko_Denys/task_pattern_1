package com.epam.controller.impl;

import com.epam.controller.Bakery;
import com.epam.enums.PizzaTypes;
import com.epam.model.Pizza;
import com.epam.model.impl.CheesePizza;
import com.epam.model.impl.ClamPizza;
import com.epam.model.impl.PeperoniPizza;
import com.epam.model.impl.VeggiePizza;

public class DniproBakery extends Bakery {

    Pizza pizza;

    @Override
    protected Pizza bakePizza(PizzaTypes pizzaTypes) {

        if(pizzaTypes == PizzaTypes.PepperoniPizza) {
            pizza = new PeperoniPizza("dough", "pork", "paprika", "garlic", "beef");
        }
        if(pizzaTypes == PizzaTypes.CheesePizza) {
           pizza = new CheesePizza("dough", "mozzarella", "parmesan", "basil");
        }
        if(pizzaTypes == PizzaTypes.ClamPizza) {
           pizza = new ClamPizza("dough", "clams topping", "mozzarella", "oregano");
        }
        if(pizzaTypes == PizzaTypes.VeggiePizza) {
           pizza = new VeggiePizza("crescent rolls", "sour cream", " cream cheese", " garlic");
        }
        return pizza;
    }
}
