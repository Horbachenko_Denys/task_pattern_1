package com.epam.controller;

import com.epam.enums.PizzaTypes;
import com.epam.model.Pizza;

public abstract class Bakery {

  protected abstract Pizza bakePizza(PizzaTypes pizzaTypes);

   public Pizza makePizza(PizzaTypes pizzaTypes) {
       Pizza pizza = bakePizza(pizzaTypes);
       pizza.make();
       pizza.bake();
       pizza.box();
       return pizza;
   }
}



